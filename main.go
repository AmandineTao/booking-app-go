package main

import (
	"fmt"
	"time"
	"sync"
)

// package level variables
const conferenceName = "Go Conference"
const conferenceTickets int = 50

var remainingTickets uint = 50
var bookings = make([]UserData, 0) // list of map of size 0

type UserData struct {
	firstName       string
	lastName        string
	email           string
	numberOfTickets uint
}

var wg = sync.WaitGroup{

}

func main() {

	greetUsers()

	// get user information
	firstName, lastName, email, userTickets := getUserInput()

	// user inputs validation
	isValidName, isValidEmail, isValidTicketNumber := validateUserInput(firstName, lastName, email, userTickets)

	if isValidName && isValidEmail && isValidTicketNumber {
		// booking and sending tickets
		bookTicket(userTickets, firstName, lastName, email)

		// add the number of thread that the main thread should wait after it execution is done (app exit)
		wg.Add(1)

		// deal with concurrency : define a thread for the app to run book and send ticket in parallel
		go sendTicket(userTickets, firstName, lastName, email)

		// Display first names of booked users
		firstNames := getFirstNames()
		fmt.Printf("The first names of bookings are: %v\n", firstNames)

		if remainingTickets == 0 {
			// end program
			fmt.Println("Our conference is booked out. Come back next year!")
		}
	} else {
		if !isValidName {
			fmt.Println("first name or last name you entered is too short.")
		}
		if !isValidEmail {
			fmt.Println("email address you entered doesn't contain @ sign.")
		}
		if !isValidTicketNumber {
			fmt.Println("number of tickets you entered is invalid.")
		}
	}
	// tell the main thread to wait for other added thread to be done before app exit
	wg.Wait()
}

func greetUsers() {

	fmt.Println("****************************************************")
	fmt.Printf("*** Welcome to %v booking application ***\n", conferenceName)
	fmt.Println("****************************************************")
	fmt.Println("  ")
	fmt.Printf("We have total of %v tickets and %v are still available.\n\n", conferenceTickets, remainingTickets)
	fmt.Println("Get your tickets here to attend.")
	fmt.Println("  ")
}

func getFirstNames() []string {
	firstNames := []string{}
	for _, booking := range bookings {
		firstNames = append(firstNames, booking.firstName)
	}
	return firstNames
}

func getUserInput() (string, string, string, uint) {
	var firstName string
	var lastName string
	var email string
	var userTickets uint

	fmt.Println("Enter your first name: ")
	fmt.Scan(&firstName)

	fmt.Println("Enter your last name: ")
	fmt.Scan(&lastName)

	fmt.Println("Enter your email address: ")
	fmt.Scan(&email)

	fmt.Println("Enter the number of tickets: ")
	fmt.Scan(&userTickets)

	return firstName, lastName, email, userTickets
}

func bookTicket(userTickets uint, firstName string, lastName string, email string) {
	remainingTickets = remainingTickets - userTickets

	// define user data from struct
	var userData = UserData{
		firstName:       firstName,
		lastName:        lastName,
		email:           email,
		numberOfTickets: userTickets,
	}

	bookings = append(bookings, userData)
	fmt.Printf("List of bookings is %v\n", bookings)

	fmt.Printf("Thank you %v %v for booking %v tickets. You will receive a confirmation email at %v\n", firstName, lastName, userTickets, email)
	fmt.Printf("%v tickets remaining for %v\n", remainingTickets, conferenceTickets)

}

func sendTicket(userTickets uint, firstName string, lastName string, email string) {
	// simulate the time for sending email
	time.Sleep(10 * time.Second)

	// simulate ticket generation
	var ticket = fmt.Sprintf("%v tickets for %v %v", userTickets, firstName, lastName)

	// simulate sending email
	fmt.Println("############################")
	fmt.Printf("Sending ticket:\n %v \nto email address %v\n", ticket, email)
	fmt.Println("############################")

	// to remove the thread from the waiting list, telling the main thread that it is done
	wg.Done()
}
